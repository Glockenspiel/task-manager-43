package ru.t1.sukhorukova.tm.exception.field;

public class EmailEmptyException extends AbstractFieldExceprion {

    public EmailEmptyException() {
        super("Error! Email is empty...");
    }

}
