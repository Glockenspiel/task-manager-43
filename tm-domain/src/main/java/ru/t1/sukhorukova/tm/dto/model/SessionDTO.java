package ru.t1.sukhorukova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.exception.entity.UserNotFoundException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "TM_SESSION")
public final class SessionDTO extends AbstractUserOwnerModelDTO {

    @NotNull
    @Column(name = "LAST_DATE")
    private Date lastDate = new Date();

    @Nullable
    @Column(name = "ROLE")
    private Role role = null;

    public SessionDTO(@NotNull final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        this.setUserId(user.getId());
    }

}
