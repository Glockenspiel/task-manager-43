package ru.t1.sukhorukova.tm.exception.field;

import org.jetbrains.annotations.NotNull;

public final class NumberIncorrectException extends AbstractFieldExceprion {

    public NumberIncorrectException() {
        super("Error! Index is incorrect...");
    }

    public NumberIncorrectException(
            @NotNull final String value,
            @NotNull final Throwable couse
    ) {
        super("Error! This value '" + value + "' is incorrect...");
    }

}
