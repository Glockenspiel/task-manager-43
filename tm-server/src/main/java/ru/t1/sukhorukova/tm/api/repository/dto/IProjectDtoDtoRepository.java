package ru.t1.sukhorukova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.model.ProjectDTO;
import ru.t1.sukhorukova.tm.enumerated.ProjectSort;

import java.util.List;

public interface IProjectDtoDtoRepository extends IUserOwnerDtoRepository<ProjectDTO> {

    @Nullable
    List<ProjectDTO> findAll(@NotNull String userId, @NotNull ProjectSort sort);

}
