package ru.t1.sukhorukova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.sukhorukova.tm.api.service.IConnectionService;
import ru.t1.sukhorukova.tm.api.service.dto.ISessionDtoService;
import ru.t1.sukhorukova.tm.dto.model.SessionDTO;
import ru.t1.sukhorukova.tm.repository.dto.SessionDtoRepository;

import javax.persistence.EntityManager;

public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO, ISessionDtoRepository> implements ISessionDtoService {

    public SessionDtoService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    public ISessionDtoRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDtoRepository(entityManager);
    }

}
