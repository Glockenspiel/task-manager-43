package ru.t1.sukhorukova.tm.api.repository.model;

import ru.t1.sukhorukova.tm.model.Session;

public interface ISessionRepository extends IUserOwnerRepository<Session> {
}
