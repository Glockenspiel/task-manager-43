package ru.t1.sukhorukova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.api.repository.model.ISessionRepository;
import ru.t1.sukhorukova.tm.api.service.IConnectionService;
import ru.t1.sukhorukova.tm.api.service.model.ISessionService;
import ru.t1.sukhorukova.tm.model.Session;
import ru.t1.sukhorukova.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    public ISessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

}
