package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.api.provider.IConnectionProvider;

public interface IPropertyService extends ISaltProvider, IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

}
